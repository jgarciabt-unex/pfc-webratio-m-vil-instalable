#
# $1-> Target directory
# $2-> Application name
# $3-> Local/Remote
# $4,$5... -> Platforms
#


currentDirectory=`pwd`

hasNode=$(node --version)

#Check if Node.js is installed. If not, finish the program.
if [[ $hasNode == [vV][0-9.]* ]];then
	echo Node is installed
else
	echo Error, "Node.js" is not installed. Please install it and try again. 
	exit 1
fi

#Check if Phonegap 3.X is installed. If not, we install it with SUDO option.
hasPhonegap=$(phonegap -v)
if [[ $hasPhonegap == 3.[0-3].[0-9.-]* ]];then
	echo Phonegap CLI is installed
else
	echo Phonegap CLI is not installed. Installing Phonegap3.3 through Node.
	sudo npm install -g phonegap@3.3
fi

#Move to target directory
cd $1

#Create Phonega project
echo Creating the project...
phonegap create $2


#Move to application folder
cd $2

#Install "Connection" plug-in on the current project.
echo Installing "Conection" plug-in...
phonegap local plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-network-information.git

#Copy all necesary files to application_name/www directoy before compile the apps.
#Delete "index.html" file because the user is not allowed to edit it.
yes | cp -r templates/* www/
yes | rm templates/index.html 
if [ -d tmp ]; then
	yes | cp tmp/icon.png www
	yes | cp -r tmp/iconWWW/* www/res/icon
fi

#Build the apps for each platforms.
echo Building apps...
for i in `seq 4 $#` 
do
	phonegap $3 build ${!i}
done	

if [ $3 == "local" ]; then
	#If the user has selected "Custom Icon", change all the apps icons before build it again.
	if [ -d tmp ]; then
		if [ -d platforms/android ]; then #If Android exist...
			yes | cp -r tmp/res platforms/android
		fi		

		if [ -d platforms/ios ]; then #If iOS exist...
			yes | cp -r tmp/icons platforms/ios/$2/Resources
		fi
		
		#Build each platform again to add the custom icons to the app in the release mode.
		for i in `seq 4 $#` 
		do
			phonegap $3 build ${!i} --release
		done
	fi

	mkdir apps
	if [ -d platforms/android ]; then #If Android exist...
		yes | cp  platforms/android/bin/*.apk apps/
	fi		
	if [ -d platforms/ios ]; then #If iOS exist...
		yes | cp -r platforms/ios/build/emulator/$2.app apps/
	fi		
	if [ -d platforms/blackberry10 ]; then #If BlackBerry exist...
		yes | cp platforms/blackberry10/build/device/bb10app.bar apps/
	fi		

fi

#Delete all temporary files
if [ -d tmp ];then
	yes | rm -r tmp
fi
yes | rm -r res
yes | rm -r scripts
	

read -p "Press any key to finish..."

exit 0