package webratio.wizard;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

 
public class MyWizard extends Wizard {

	Page1 page1 = new Page1();
	Page2 page2 = new Page2();
	Page3 page3 = new Page3();
	
	public MyWizard() {
		setWindowTitle("WebRatio Movil Instalable - Setup Wizard");
	}

	@Override
	public void addPages() {
		addPage(page1);
		addPage(page2);
		addPage(page3);
	}

public String getParams(){
	String params=null;

	params=page1.getDirectory()+" "+page2.getAppName();
	params+=page2.getCompile()+page2.getPlattforms();
	return params;
}


public String getURL(){	
	return page2.getURL();
}



 @Override
 public boolean performFinish() {
	 return  page1.isPageComplete() && page2.isPageComplete() && page3.isPageComplete();
 }
 
	public static void main(String[] args) {
		
		Display display = new Display();
		final Shell shell = new Shell(display);
		MyWizard customWizard = new MyWizard();
		WizardDialog dialog = new WizardDialog(shell, customWizard);
		dialog.open(); 
		
	}
 
}