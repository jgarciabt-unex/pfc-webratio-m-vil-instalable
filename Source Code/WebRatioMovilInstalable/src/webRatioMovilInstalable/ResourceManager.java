package webRatioMovilInstalable;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;


enum osType { WINDOWS , UNIX };

public class ResourceManager {

	private osType os = null;
	private String command;
	private String directory;
	private String appName;
	private String url;
	private String icoPath; 			 // 
	private String offlineResourcesPath; // This attributes could be null
	private String offlineName;  		 //
	private String configPath;			 //
	static ManageFile FileManager;

	
	public ResourceManager(){
		String system;
		
		system = System.getProperty("os.name");
		if(system.startsWith("Windows"))
			os=osType.WINDOWS;
		else
			os=osType.UNIX;
	}
	
	public void setCommand(String _command){
		command=_command;
	}
	
	public void setDirectory(String _directory){
		directory=_directory;
	}
	
	public void setAppName(String _appName){
		appName=_appName;
	}
	
	public void setUrl(String _url){
		url=_url;
	}
	
	public void setIcoPath(String _path){
		icoPath=_path;
	}
	
	public void setOfflineResourcePath(String _path){
		
		if(_path!=""){
			File offline = new File (_path);
			offlineResourcesPath=offline.getParent();
			offlineName=offline.getName();
		}
		else{
			offlineResourcesPath="";
			offlineName="offline.html";
		}
	}
	
	public void setConfigPat(String _path){
		configPath=_path;
	}
	
	public void execute() throws IOException{
		String comando;
		String dir=getResourcesPath();
        Runtime runtime; 
	    Process process;
		FileManager = new ManageFile();

		//Scripts and Templates folders creation
		File folder = new File(directory+File.separator+"scripts");
		folder.mkdirs();
        folder = new File(directory+File.separator+"templates");
        folder.mkdirs();
    	
        getResourcesFromJar(directory);
        
        createConfig(dir);
        createOffline(dir);
        createIndex(dir);

        //If the user specify it, this block will create the icons for all platforms
        if(icoPath!=""){
        	folder = new File(directory+File.separator+"tmp");
        	folder.mkdirs();
        	androidIcons(directory, icoPath);
        	iosIcons(directory, icoPath);
        	wwwIcons(directory, icoPath);
        	resizeImg(icoPath, directory+File.separator+"tmp"+File.separator+"icon.png", 128);
        }
     
        if(os==osType.UNIX){//Unix
        	//Creation of "launcher.sh" file. This file will launch the main script
        	FileManager.setOutName(directory+File.separator+"scripts"+File.separator+"launcher.sh");
        	FileManager.openWriter(false);
        	FileManager.writeln("echo Executing launcher.sh");
        	FileManager.writeln("cd "+dir);
        	FileManager.writeln("sh main.sh "+command);
        	FileManager.closeWriter();
        	File launcher = new File(directory+File.separator+"scripts"+File.separator+"launcher.sh");
        	launcher.setExecutable(true);
        
        	//Command to execute "launcher.sh"
        	comando ="open -a Terminal "+directory+File.separator+"scripts"+File.separator+"launcher.sh";
        }
        else{//Windows
        	//Creation of "launcher.bat" file. This file will launch the main script
        	FileManager.setOutName(directory+File.separator+"scripts"+File.separator+"launcher.bat");
        	FileManager.openWriter(false);
        	FileManager.writeln("echo Executing launcher.bat");
        	FileManager.writeln("cmd /k "+dir+File.separator+"main.bat "+command);
          	FileManager.closeWriter();
        
          	//This command is executed to check the phonegap's version. For Windows version only.
          	comando="cmd /c phonegap --version > "+dir+File.separator+"version.tmp";
            runtime = Runtime.getRuntime(); 
    	    process = runtime.exec(comando);
    	    try {
				process.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
          	
    	    //Command to execute "launcher.bat"
        	comando ="cmd /c start cmd /c "+directory+File.separator+"scripts"+File.separator+"launcher.bat";
        }
        
        runtime = Runtime.getRuntime(); 
	    process = runtime.exec(comando);

	    try {
			process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
	
	//This method extracts the resources necessary from JAR files
	private void getResourcesFromJar(String destDir) throws IOException{
		
		String resource = "res";
		File jarFile = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
	
		if(jarFile.isFile()) { 
		    JarFile jar = new JarFile(jarFile);
		    final Enumeration<JarEntry> entries = jar.entries();
		    do {    
		        JarEntry file = (JarEntry) entries.nextElement();
		        String name = file.getName();
		        if (name.startsWith(resource +"/")) { 
		            File f = new File(destDir + File.separator + file.getName());
		            if (file.isDirectory()) { 
		            	f.mkdir();
		            }
		            else{
		            	InputStream is = jar.getInputStream(file);
		            	FileOutputStream fos = new FileOutputStream(f);
		            	while (is.available() > 0) { 
		            		fos.write(is.read());
		            	}
		            	fos.close();
		            	is.close();
		            }
		        }
		    }while(entries.hasMoreElements());
		    jar.close();
		}
		
	}
 	
	private String getResourcesPath(){

		return directory+File.separator+"res";
	}
	
	//Creation of "Index.html" file, with destination's URL and link to offline page.
	private void createIndex(String path){
		
        FileManager.setInName(path+File.separator+"index1.html");
        FileManager.setOutName(directory+File.separator+"templates"+File.separator+"index.html");
        FileManager.openReader();
        FileManager.openWriter(false);
        FileManager.concatenate();
        FileManager.writeln("			window.location.href='http://"+url+"';");
        FileManager.writeln("			}\n\n	function offLineConnection(){");
        FileManager.writeln("			window.location.href='./"+offlineName+"';");
        FileManager.closeReader();
        FileManager.setInName(path+File.separator+"index2.html");
        FileManager.openReader();
        FileManager.concatenate();
        FileManager.closeReader();
        FileManager.closeWriter();
        
	}
	
	//This method creates or loads the offline page.
	private void createOffline(String path) throws IOException{
		//If the user doesn't specify , this block will create a default offline page
        if(offlineResourcesPath==""){
        	FileManager.setInName(path+File.separator+"offline1.html");
        	FileManager.setOutName(directory+File.separator+"templates"+File.separator+"offline.html");
        	FileManager.openReader();
        	FileManager.openWriter(false);
        	FileManager.concatenate();
        	FileManager.writeln("			window.location.href='http://"+url+"';");
        	FileManager.closeReader();
        	FileManager.setInName(path+File.separator+"offline2.html");
        	FileManager.openReader();
        	FileManager.concatenate();
        	FileManager.closeReader();
        	FileManager.closeWriter();
        	
        	File src=new File(path+File.separator+"index.css");
        	File dest=new File(directory+File.separator+"templates"+File.separator+"index.css");
        	FileUtils.copyFile(src,dest);
        }
        else{ //If the user specify, this block will load the resources for he offline page
        	File src=new File(offlineResourcesPath);
        	File dest=new File(directory+File.separator+"templates");
        	dest.mkdir();
        	FileUtils.copyDirectory(src,dest);
        }
	}
	
	//This method creates or loads the configuration file "config.xml"
	private void createConfig(String path) throws IOException{
		
		if(configPath==""){
			FileManager.setInName(path+File.separator+"config1.xml");
        	FileManager.setOutName(directory+File.separator+"templates"+File.separator+"config.xml");
        	FileManager.openReader();
        	FileManager.openWriter(false);
        	FileManager.concatenate();
        	FileManager.writeln("	<name>"+appName+"</name>");
        	FileManager.closeReader();
        	FileManager.setInName(path+File.separator+"config2.xml");
        	FileManager.openReader();
        	FileManager.concatenate();
        	FileManager.closeReader();
        	FileManager.closeWriter();
		}
		else{
			File src = new File(configPath+File.separator+"config.xml");
			File dst = new File(directory+File.separator+"templates"+File.separator+"config.xml");
			FileUtils.copyFile(src, dst);
		}
	}
	
	//This method resizes "size"pixels the image located in "src", and save it in "dst"
	private static void resizeImg(String src, String dst, int size){
		
		BufferedImage img;
		
		try{
		    img = ImageIO.read(new File(src));
			BufferedImage thumbnail = Scalr.resize(img, size);
			File outputfile = new File(dst);
			ImageIO.write(thumbnail, "png", outputfile);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	//This method creates all icons versions for all platforms used in this project
	private void wwwIcons(String path, String resourcePath){
		
		File folderResources = new File(path+File.separator+"tmp"+File.separator+"iconWWW");
		folderResources.mkdirs();
		
		//Android
		File platformFolder = new File(path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"android");
		platformFolder.mkdirs();
		
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"android"+File.separator+"icon-36-ldpi.png", 36);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"android"+File.separator+"icon-48-mdpi.png", 48);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"android"+File.separator+"icon-72-hdpi.png", 72);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"android"+File.separator+"icon-96-xhdpi.png", 96);
      
        //iOS
        platformFolder = new File(path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"ios");
		platformFolder.mkdirs();
		
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"ios"+File.separator+"icon-72.png", 72);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"ios"+File.separator+"icon-72-2x.png", 144);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"ios"+File.separator+"icon-57.png", 52);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"ios"+File.separator+"icon-57-2x.png", 114);
		
        //blackberry
        platformFolder = new File(path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"blackberry");
		platformFolder.mkdirs();
		
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"blackberry"+File.separator+"icon-80.png", 80);
     
        //windows-phone
        platformFolder = new File(path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"windows-phone");
		platformFolder.mkdirs();
		
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"windows-phone"+File.separator+"icon-48.png", 48);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"windows-phone"+File.separator+"icon-62-tile.png", 62);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"iconWWW"+File.separator+"windows-phone"+File.separator+"icon-173.png", 173);
		
		
	}
	
	private void androidIcons(String path, String resourcePath){
		
        File platformFolder = new File(path+File.separator+"tmp"+File.separator+"res");
        platformFolder.mkdirs();
        
        File subFolder = new File(path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-xhdpi");
        subFolder.mkdirs();
        subFolder = new File(path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-hdpi");
        subFolder.mkdirs();
        subFolder = new File(path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-mdpi");
        subFolder.mkdirs();
        subFolder = new File(path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-ldpi");
        subFolder.mkdirs();
        subFolder = new File(path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable");
        subFolder.mkdirs();
        
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-xhdpi"+File.separator+"icon.png", 96);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-hdpi"+File.separator+"icon.png", 72);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-mdpi"+File.separator+"icon.png", 48);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable-ldpi"+File.separator+"icon.png", 36);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"res"+File.separator+"drawable"+File.separator+"icon.png", 96);
		
	}
	
	private void iosIcons(String path, String resourcePath){
		
        File platformFolder = new File(path+File.separator+"tmp"+File.separator+"icons");
        platformFolder.mkdirs();     
        
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-40.png", 40);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-40@2x.png", 80);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-50.png", 50);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-50@2x.png", 100);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-60.png", 60);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-60@2x.png", 60);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-72.png", 72);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-72@2x.png", 144);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-76.png", 76);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-76@2x.png", 152);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-small.png", 29);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon-small@2x.png", 58);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon.png", 57);
        resizeImg(resourcePath, path+File.separator+"tmp"+File.separator+"icons"+File.separator+"icon@2x.png", 114);
	}
	
}
