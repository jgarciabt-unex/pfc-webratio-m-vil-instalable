package webRatioMovilInstalable;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


public class Page2 extends WizardPage {
	
	public static final String PAGE_NAME = "Configuración";
	
	private Text URLField;
	private Text appNameField;
	private Button btnRemoto;
	private Button btnLocal;
	private Button btnAndLocal;
	private Button btnIosLocal;
	private Button btnBBLocal;
	private Button btnWP7Local;
	private Button btnWP8Local;
	private Button btnAndRemoto;
	private Button btnIosRemoto;
	private Button btnWP8Remoto;

	private boolean hasName;
	private boolean hasUrl;
	private boolean hasLocal;
	private boolean hasRemote;
	private Composite composite_4;

	
	protected Page2() {
		super(PAGE_NAME, "App settings", null);
		setPageComplete(false);
		
		hasUrl=false;
		hasLocal=false;
		hasRemote=false;

	}

	@Override
	public void createControl(Composite arg0) {
		
		Composite container = new Composite(arg0,SWT.NONE);
		container.setLayout(new GridLayout());
		container.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		
		composite_4 = new Composite(container, SWT.NONE);
		GridData gd_composite_4 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_composite_4.widthHint = 542;
		composite_4.setLayoutData(gd_composite_4);
		
		Label lblNombreDeLa = new Label(composite_4, SWT.NONE);
		lblNombreDeLa.setText("App name:");
		lblNombreDeLa.setBounds(4, 13, 64, 14);

		
		appNameField = new Text(composite_4, SWT.BORDER);
		appNameField.setBounds(74, 10, 110, 19);
		
		Label despliegueLabel = new Label(composite_4, SWT.NONE);
		despliegueLabel.setLocation(234, 13);
		despliegueLabel.setSize(101, 21);
		despliegueLabel.setText("Deployment URL:");
		
		URLField = new Text (composite_4,SWT.BORDER|SWT.SINGLE);
		URLField.setLocation(338, 10);
		URLField.setSize(160, 19);
		URLField.setMessage("www.example.com");
		URLField.addKeyListener(new KeyListener(){
		   public void keyPressed(KeyEvent e) {
		    }

		    public void keyReleased(KeyEvent e) {
		      if (!URLField.getText().isEmpty()) {
		        btnLocal.setEnabled(true);
		        btnRemoto.setEnabled(true);
		        hasUrl=true;
		       }
		       else{
		        btnLocal.setEnabled(false);
		        btnRemoto.setEnabled(false);
		        changeStateLocal(false);
		        changeStateRemote(false);
		        hasUrl=false;
		       }
		       checkStatus();
		    }

		    });
		
	    //Control nombre de la aplicacion
        appNameField.addKeyListener(new KeyListener(){
            public void keyPressed(KeyEvent e) {
              }
              public void keyReleased(KeyEvent e) {
                if (!appNameField.getText().isEmpty()) {
                   	hasName=true;
                }
                else{
                    hasName=false;
                }
                checkStatus();
              }

          });
		
		Composite composite = new Composite(container, SWT.NONE);
		GridData gd_composite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_composite.heightHint = 279;
		gd_composite.widthHint = 543;
		composite.setLayoutData(gd_composite);
		
		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setBounds(0, 10, 541, 29);
		
		Label dondeLabel = new Label(composite_2, SWT.NONE);
		dondeLabel.setLocation(10, 10);
		dondeLabel.setSize(428, 19);
		dondeLabel.setText("Compilation type:");
		
		Composite composite_3 = new Composite(composite, SWT.NONE);
		composite_3.setBounds(0, 45, 541, 223);
		
		Label label = new Label(composite_3, SWT.SEPARATOR | SWT.VERTICAL);
		label.setBounds(232, 10, 2, 156);
		
		btnLocal = new Button(composite_3, SWT.RADIO);
		btnLocal.setBounds(38, 20, 91, 18);
		btnLocal.setText("Local*");
		btnLocal.setEnabled(false);
		
		btnRemoto = new Button(composite_3, SWT.RADIO);
		btnRemoto.setBounds(288, 21, 91, 18);
		btnRemoto.setText("Remote**");
		btnRemoto.setEnabled(false);
		
		btnAndLocal = new Button(composite_3, SWT.CHECK);
		btnAndLocal.setEnabled(false);
		btnAndLocal.setBounds(26, 50, 93, 18);
		btnAndLocal.setText("Android");
		btnAndLocal.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasLocal=checkLocalStatus();
				checkStatus();
			}
			});
		
		btnIosLocal = new Button(composite_3, SWT.CHECK);
		btnIosLocal.setEnabled(false);
		btnIosLocal.setBounds(26, 75, 93, 18);
		btnIosLocal.setText("iOS");
		btnIosLocal.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasLocal=checkLocalStatus();
				checkStatus();
			}
			});
		
		btnBBLocal = new Button(composite_3, SWT.CHECK);
		btnBBLocal.setEnabled(false);
		btnBBLocal.setBounds(26, 100, 131, 18);
		btnBBLocal.setText("BlackBerry 10");
		btnBBLocal.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasLocal=checkLocalStatus();
				checkStatus();
			}
			});
		
		btnWP7Local = new Button(composite_3, SWT.CHECK);
		btnWP7Local.setEnabled(false);
		btnWP7Local.setBounds(26, 148, 131, 18);
		btnWP7Local.setText("Windows Phone 7");
		btnWP7Local.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasLocal=checkLocalStatus();
				checkStatus();
			}
			});	
		
		btnWP8Local = new Button(composite_3, SWT.CHECK);
		btnWP8Local.setEnabled(false);
		btnWP8Local.setBounds(26, 124, 131, 18);
		btnWP8Local.setText("Windows Phone 8");
		btnWP8Local.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasLocal=checkLocalStatus();
				checkStatus();
			}
			});
		
		//Habilitamos/deshabilitamos las opciones locales
		btnLocal.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
			if (btnLocal.getSelection()) {
				changeStateLocal(true);
				changeStateRemote(false);
				checkStatus();
			}
			}
			});
		
		
		btnAndRemoto = new Button(composite_3, SWT.CHECK);
		btnAndRemoto.setEnabled(false);
		btnAndRemoto.setBounds(277, 51, 93, 18);
		btnAndRemoto.setText("Android");
		btnAndRemoto.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasRemote=checkRemoteStatus();
				checkStatus();
			}
			});
		
		btnIosRemoto = new Button(composite_3, SWT.CHECK);
		btnIosRemoto.setEnabled(false);
		btnIosRemoto.setBounds(277, 76, 93, 18);
		btnIosRemoto.setText("iOS");
		btnIosRemoto.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasRemote=checkRemoteStatus();
				checkStatus();
			}
			});
		
		btnWP8Remoto = new Button(composite_3, SWT.CHECK);
		btnWP8Remoto.setEnabled(false);
		btnWP8Remoto.setBounds(277, 100, 131, 18);
		btnWP8Remoto.setText("Windows Phone 8");
		
		Label lblrequiereTenerInstalados = new Label(composite_3, SWT.NONE);
		lblrequiereTenerInstalados.setBounds(10, 183, 549, 14);
		lblrequiereTenerInstalados.setText("*Requires to install the SDK for every platform you need to use.");
		
		Label lblrequiereDisponerDe = new Label(composite_3, SWT.NONE);
		lblrequiereDisponerDe.setBounds(10, 203, 549, 14);
		lblrequiereDisponerDe.setText("**Requires to have an account in the Phonegap Build service.");
		btnWP8Remoto.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
				hasRemote=checkRemoteStatus();
				checkStatus();
			}
			});
		
		btnRemoto.addMouseListener(new MouseAdapter() {
			public void mouseUp(MouseEvent e) {
			if (btnRemoto.getSelection()) {
				changeStateRemote(true);			
				changeStateLocal(false);
				checkStatus();
			}
			}
			});
		
		setControl(container);
	}
	
	private void changeStateLocal(boolean estado){
		//Habilitamos o Deshabilitamos
		btnAndLocal.setEnabled(estado);
		btnIosLocal.setEnabled(estado);
		btnBBLocal.setEnabled(estado);
		btnWP7Local.setEnabled(estado);
		btnWP8Local.setEnabled(estado);	
		//Deseleccionamos en caso de deshabilitados
		if(estado==false){
			btnAndLocal.setSelection(estado);
			btnIosLocal.setSelection(estado);
			btnBBLocal.setSelection(estado);
			btnWP7Local.setSelection(estado);
			btnWP8Local.setSelection(estado);	
			hasLocal=false;
		}
	}
	
	private void changeStateRemote(boolean estado){
		//Habilitamos o Deshabilitamos
		btnAndRemoto.setEnabled(estado);
		btnIosRemoto.setEnabled(estado);
		btnWP8Remoto.setEnabled(estado);	
		//Desseleccionamos en caso de deshabilitados
		if(estado==false){
			btnAndRemoto.setSelection(estado);
			btnIosRemoto.setSelection(estado);
			btnWP8Remoto.setSelection(estado);
			hasRemote=false;
		}
	}
	
	public String getURL(){
		return URLField.getText();
	}
	
	public String getAppName(){
		return appNameField.getText();
	}
	
	public String getCompile(){
		if(btnLocal.getSelection()){
			return " local";
		}
		else{
			return " remote";
		}
	}
	
	public String getPlattforms(){
		String platforms="";
		if(btnAndLocal.getSelection()||btnAndRemoto.getSelection()){
			platforms+=" android";
		}
		if(btnIosLocal.getSelection()||btnIosRemoto.getSelection()){
			platforms+=" ios";
		}
		if(btnBBLocal.getSelection()){
			platforms+=" blackberry";
		}
		if(btnWP7Local.getSelection()){
			platforms+=" wp7";
		}
		if(btnWP8Local.getSelection()||btnWP8Remoto.getSelection()){
			platforms+=" wp8";
		}
		
		return platforms;
	}
	
	private boolean checkLocalStatus(){
		return btnAndLocal.getSelection()||btnIosLocal.getSelection()||btnBBLocal.getSelection()||btnWP7Local.getSelection()||btnWP8Local.getSelection();
	}
	
	private boolean checkRemoteStatus(){
		return btnAndRemoto.getSelection()||btnIosRemoto.getSelection()||btnWP8Remoto.getSelection();
	}
	
	private void checkStatus() {
		String appName,url,comp,platf,directory,ico,offlineFile,configDirectory;
		boolean complete=false;
		
		complete=canFlipToNextPage();
		setPageComplete(complete);
		//explicit call
		getWizard().getContainer().updateButtons();
		
		//Actualizamos el contenido de la ventana resumen
		Page1 p1 = (Page1) getWizard().getPreviousPage(this);
		directory=p1.getDirectory();
		if(p1.getSelectionConfig())
			configDirectory=p1.getConfig();
		else
			configDirectory="Default";
		if(p1.getSelectionIco())
			ico=p1.getIco();
		else
			ico="Default";
		if(p1.getSelectionHTML()){
			offlineFile=p1.getOfflineResources();
		}
		else{
			offlineFile="Default";
		}
			
		appName=getAppName();
		url=getURL();
		comp=getCompile();
		platf=getPlattforms();
		Page3 p3 = (Page3) getWizard().getNextPage(this);
		p3.setLabels(appName,url,comp,platf,directory,ico,offlineFile,configDirectory);
	 }
		  
		  @Override
	public boolean canFlipToNextPage() {
		return hasUrl&&(hasLocal||hasRemote)&&hasName;
	}
}