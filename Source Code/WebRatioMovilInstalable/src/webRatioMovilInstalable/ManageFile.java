package webRatioMovilInstalable;

import java.io.*;

public class ManageFile
{
	String outName = null;
	String inName = null;
    FileWriter out = null;
    FileReader in = null;
    PrintWriter pw = null;
    BufferedReader br = null;
    
    public void setOutName(String fileName){
    	outName = fileName;
    }
    
    public void setInName(String fileName){
    	inName = fileName;
    }
    public void openWriter(boolean append){
    	try {
			out = new FileWriter(outName,append);
	        pw = new PrintWriter(out);
		} catch (IOException e) {
			e.printStackTrace();
		}

    }
    
    public void openReader(){
    	try {
			in = new FileReader(inName);
			br = new BufferedReader(in);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
    
    public void writeln(String str){
    	pw.println(str);
    }
    
    public void closeWriter(){
    	
 		try{
 			if(out!=null){
				out.close();
			}
 		}catch(IOException e){
			e.printStackTrace();
    	}
    }
    
    public void closeReader(){
    	
 		try{
 			if(in!=null){
				in.close();
			}
 		}catch (IOException e){
			e.printStackTrace();
    	}
    } 
    
    public void concatenate(){
    	String line;
    	
    	try {
			while((line=br.readLine())!=null)
			    pw.println(line);	
    	}catch (IOException e){
			e.printStackTrace();
		}

    	
    }
   
}