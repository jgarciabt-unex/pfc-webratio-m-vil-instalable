package webRatioMovilInstalable;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class Page1 extends WizardPage {
	 public static final String PAGE_NAME = "Resources settings";

	  private Text destinationField;
	  private Button fileExplorerBtn;
	  private Button customConfig;
	  private Button configExplorerBtn;
	  private Text configField;
	  private Button customIco;
	  private Button icoExplorerBtn;
	  private Text icoField;
	  private Button customHTML;
	  private Text offlineResourcesField;
	  private Button htmlExplorerBtn;
	  
	  private Label offlineDirectoryLbl;
	  private Label configMsgLbl;

	  private boolean nextDest;
	  private boolean nextConfig;
	  private boolean nextIco;
	  private boolean nextOffline1;

	  public Page1() {
	    super(PAGE_NAME, "Resources settings", null);
	    setPageComplete(false);
	    
	    nextDest=false;
	    nextConfig=false;
	    nextIco=false;
	    nextOffline1=false;
	    
	  }

	  public void createControl(Composite parent) {
		  
	    Composite topLevel = new Composite(parent, SWT.NONE);
	    topLevel.setLayout(new GridLayout(1, false));
	    
	    setControl(topLevel);
	    
	    Composite composite = new Composite(topLevel, SWT.NONE);
	    GridData gd_composite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
	    gd_composite.heightHint = 39;
	    gd_composite.widthHint = 578;
	    composite.setLayoutData(gd_composite);
	    
	    Label l = new Label(composite, SWT.CENTER);
	    l.setLocation(-2, 18);
	    l.setSize(53, 16);
	    l.setText("Target:");
	        
	    destinationField = new Text(composite, SWT.BORDER| SWT.SINGLE);
	    destinationField.setEditable(false);
	    destinationField.setLocation(59, 15);
	    destinationField.setSize(406, 19);
	            
	            
	    fileExplorerBtn = new Button(composite, SWT.BORDER);
	    fileExplorerBtn.setLocation(468, 11);
	    fileExplorerBtn.setSize(100, 28);
	    fileExplorerBtn.setText("Browse...");
	            
	    fileExplorerBtn.addMouseListener(new MouseAdapter(){
	        public void mouseDown(MouseEvent e){
	             DirectoryDialog dialogExplorer = new DirectoryDialog(getShell());

	             dialogExplorer.setFilterPath(destinationField.getText());
	             dialogExplorer.setText("Select target directory...");
	             dialogExplorer.setMessage("Select one directory");
	             String path = dialogExplorer.open();
	             if (path != null) {
	                 destinationField.setText(path);
	                 nextDest=true;
	              }
	              else
	                 nextDest=false;
	              checkStatus();
	            }
	        });
	            
	        Composite composite_1 = new Composite(topLevel, SWT.NONE);
	        GridData gd_composite_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
	        gd_composite_1.widthHint = 574;
	        gd_composite_1.heightHint = 64;
	        composite_1.setLayoutData(gd_composite_1);
	            
	        //---- Custom config section ----//
	        customConfig = new Button(composite_1, SWT.CHECK);
	        customConfig.setBounds(0, 0, 354, 18);
	        customConfig.setText("I want to use my own setup file");
	        
	        configField = new Text(composite_1, SWT.BORDER);
	        configField.setEnabled(false);
	        configField.setEditable(false);
	        configField.setBounds(20, 35, 442, 19);
	        
	        configExplorerBtn = new Button(composite_1, SWT.NONE);
	        configExplorerBtn.setEnabled(false);
	        configExplorerBtn.setBounds(468, 31, 100, 28);
	        configExplorerBtn.setText("Browse...");
	        
	        configMsgLbl = new Label(composite_1, SWT.NONE);
	        configMsgLbl.setBounds(20, 16, 442, 14);
	        configMsgLbl.setText("Please, select the directory where the CONFIG.XML is located");
	        configMsgLbl.setVisible(false);
	        
	        customConfig.addMouseListener(new MouseAdapter(){
	            public void mouseUp(MouseEvent e){
	            	if(customConfig.getSelection()){
	            		configField.setEnabled(true);
	            		configExplorerBtn.setEnabled(true);
	            		configMsgLbl.setVisible(true);
	            	}
	            	else{
	                    configField.setEnabled(false);
	                    configField.setText("");
	                    configExplorerBtn.setEnabled(false);
	                    configMsgLbl.setVisible(false);
	                    nextConfig=false;
	            	}
	            	checkStatus();
	             }
	         });
	        
	        configExplorerBtn.addMouseListener(new MouseAdapter(){
	            public void mouseDown(MouseEvent e){
	                 DirectoryDialog dialogExplorer = new DirectoryDialog(getShell());
	                 dialogExplorer.setFilterPath(destinationField.getText());
	                 dialogExplorer.setText("Select one directory...");
	                 dialogExplorer.setMessage("Please, select the directory where the \"CONFIG.XML\" is located");
	                 String path = dialogExplorer.open();
	                 if (path != null) {
	                     configField.setText(path);
	                     nextConfig=true;
	                  }
	                  else{
	                     nextConfig=false;
	                  }
	                  checkStatus();
	                        
	                }
	            });
	        
	        Composite composite_2 = new Composite(topLevel, SWT.NONE);
	        GridData gd_composite_2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
	        gd_composite_2.heightHint = 58;
	        gd_composite_2.widthHint = 577;
	        composite_2.setLayoutData(gd_composite_2);
	            
	        //---- Custom icon section ----//
	        icoExplorerBtn = new Button(composite_2, SWT.NONE);
	        icoExplorerBtn.setEnabled(false);
	        icoExplorerBtn.setLocation(468, 25);
	        icoExplorerBtn.setSize(100, 28);
	        icoExplorerBtn.setText("Browse...");
	            
	        icoField = new Text(composite_2, SWT.BORDER);
	        icoField.setEditable(false);
	        icoField.setEnabled(false);
	        icoField.setBounds(20, 29, 442, 19);
	        
	        customIco = new Button(composite_2, SWT.CHECK);
	        customIco.setBounds(0, 0, 220, 18);
	        customIco.setText("I want to use my own app icon");
	        
	        customIco.addMouseListener(new MouseAdapter(){
	            public void mouseUp(MouseEvent e){
	            	if(customIco.getSelection()){
	            		icoField.setEnabled(true);
	            		icoExplorerBtn.setEnabled(true);
	            	}
	            	else{
	                    icoField.setEnabled(false);
	                    icoField.setText("");
	                    icoExplorerBtn.setEnabled(false);
	                    nextIco=false;
	            	}
	            	checkStatus();
	             }
	         });
	        
	        icoExplorerBtn.addMouseListener(new MouseAdapter(){
	            public void mouseDown(MouseEvent e){
	                 FileDialog dialogExplorer = new FileDialog(getShell());
	                 dialogExplorer.setFilterPath(destinationField.getText());
	                 dialogExplorer.setText("Select a file...");
	                 String[] filterExt = { "*.png"};
	                 dialogExplorer.setFilterExtensions(filterExt);

	                 String path = dialogExplorer.open();
	                 if (path != null) {
	                   icoField.setText(path);
	                   nextIco=true;
	                 }
	                 else
	                   nextIco=false;
	                 checkStatus();
	             }
	         });
	        
	        //---- Custom HTML section ----//
	        Composite composite_3 = new Composite(topLevel, SWT.NONE);
	        GridData gd_composite_3 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
	        gd_composite_3.widthHint = 579;
	        gd_composite_3.heightHint = 106;
	        composite_3.setLayoutData(gd_composite_3);
	        
	        customHTML = new Button(composite_3, SWT.CHECK);
	        customHTML.setBounds(0, 0, 309, 18);
	        customHTML.setText("I want to use my own offline web (HTML)");
	        
	        customHTML.addMouseListener(new MouseAdapter(){
	            public void mouseUp(MouseEvent e){
	            	if(customHTML.getSelection()){
	            		offlineResourcesField.setEnabled(true);
	            		htmlExplorerBtn.setEnabled(true);
	            		offlineDirectoryLbl.setEnabled(true);
	            	}
	            	else{
	                    offlineResourcesField.setEnabled(false);
	                    offlineResourcesField.setText("");
	                    htmlExplorerBtn.setEnabled(false);
	            		nextOffline1=false;
	            		offlineDirectoryLbl.setEnabled(false);
	            	}
	            	checkStatus();
	             }
	         });
	        
	        offlineResourcesField = new Text(composite_3, SWT.BORDER);
	        offlineResourcesField.setEditable(false);
	        offlineResourcesField.setEnabled(false);
	        offlineResourcesField.setBounds(18, 64, 444, 19);
	        
	        htmlExplorerBtn = new Button(composite_3, SWT.NONE);
	        htmlExplorerBtn.setEnabled(false);
	        htmlExplorerBtn.setBounds(469, 60, 100, 28);
	        htmlExplorerBtn.setText("Browse...");
	        
	        offlineDirectoryLbl = new Label(composite_3, SWT.NONE);
	        offlineDirectoryLbl.setEnabled(false);
	        offlineDirectoryLbl.setBounds(20, 24, 450, 34);
	        offlineDirectoryLbl.setText("Please notice that all files located in the same directory as the selected file\nwill be included into the App");
	        
	        htmlExplorerBtn.addMouseListener(new MouseAdapter(){
	                public void mouseDown(MouseEvent e){
	                    FileDialog dialogExplorer = new FileDialog(getShell());
	                    dialogExplorer.setFilterPath(destinationField.getText());
	                    dialogExplorer.setText("Select the HTML file that will work as a main offline web");
	                    String[] filterExt = { "*.html"};
	                    dialogExplorer.setFilterExtensions(filterExt);

	                    String path = dialogExplorer.open();
	                    if (path != null) {
	                      offlineResourcesField.setText(path);
	                      nextOffline1=true;
	                    }
	                    else
	                      nextOffline1=false;
	                    checkStatus();
	                }
	           });
	        
	         
	  }

	  public String getDirectory() { 
	    return destinationField.getText();
	  }
	  
	  public boolean getSelectionConfig(){
		  return customConfig.getSelection();
	  }
	  
	  public String getConfig(){
		  return configField.getText();
	  }
	  
	  public boolean getSelectionIco(){
		  return customIco.getSelection();
	  }
	  
	  public String getIco(){
		  return icoField.getText();
	  }
	  
	  public boolean getSelectionHTML(){
		  return customHTML.getSelection();
	  }
	  
	  public String getOfflineResources(){
		  return offlineResourcesField.getText();
	  }

	  
	  private void checkStatus() {
		  
		  boolean complete=false;
		  
		  complete=canFlipToNextPage();
		  setPageComplete(complete);
		  getWizard().getContainer().updateButtons();

	  }
	  
	  @Override
	  public boolean canFlipToNextPage() {
		  
		  boolean canFlip=true;
		  
		  if(customConfig.getSelection())
			  canFlip=canFlip && nextConfig;
		  if(customIco.getSelection())
			  canFlip=canFlip && nextIco;
		  if(customHTML.getSelection())
			  canFlip=canFlip && nextOffline1;
		  
		  return canFlip && nextDest;
		  
	  }
	}