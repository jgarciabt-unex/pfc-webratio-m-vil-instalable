package webRatioMovilInstalable;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;


public class Page3 extends WizardPage {
	public static final String PAGE_NAME = "Settings summary";
	

	private Label appNameLabel;
	private Label directoryLabel;
	private Label urlLabel;
	private Label compilationLabel;
	private Label platformsLabel;
	private Label iconLabel;
	private Label offlineDirectoryLabel;
	private Label configDirectoryLabel;


	protected Page3() {
		super(PAGE_NAME, "Settings summary", null);
		setPageComplete(false);

	}

@Override
public void createControl(Composite parent) {
	  Composite topLevel = new Composite(parent, SWT.NONE);
	    topLevel.setLayout(new GridLayout(1, false));
	    
	    setControl(topLevel);
	    

	    Composite composite = new Composite(topLevel, SWT.BORDER);
	    composite.setToolTipText("Symmary:");
	    GridData gd_composite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
	    gd_composite.widthHint = 582;
	    gd_composite.heightHint = 268;
	    composite.setLayoutData(gd_composite);
	    
	    Label lblNombreDeLa = new Label(composite, SWT.NONE);
	    lblNombreDeLa.setBounds(10, 10, 147, 24);
	    lblNombreDeLa.setText("App's Name:");
	    
	    Label lblNewLabel = new Label(composite, SWT.NONE);
	    lblNewLabel.setBounds(10, 40, 147, 24);
	    lblNewLabel.setText("Deployment URL:");
	    
	    Label lblTipoDeCompilacin = new Label(composite, SWT.NONE);
	    lblTipoDeCompilacin.setBounds(10, 70, 147, 24);
	    lblTipoDeCompilacin.setText("Compilation type:");
	    
	    Label lblPlataformasSeleccionadas = new Label(composite, SWT.NONE);
	    lblPlataformasSeleccionadas.setBounds(10, 100, 147, 24);
	    lblPlataformasSeleccionadas.setText("Selected platforms:");
	    
	    Label lblDirectorioDestino = new Label(composite, SWT.NONE);
	    lblDirectorioDestino.setBounds(10, 130, 147, 24);
	    lblDirectorioDestino.setText("Target directory:");
	    
	    Label lblIconoDeLa = new Label(composite, SWT.NONE);
	    lblIconoDeLa.setBounds(10, 190, 147, 24);
	    lblIconoDeLa.setText("App's icon:");
	    
	    Label lblPaginaOffline = new Label(composite, SWT.NONE);
	    lblPaginaOffline.setBounds(10, 220, 147, 24);
	    lblPaginaOffline.setText("Offline page:");
	    
	    appNameLabel = new Label(composite, SWT.NONE);
	    appNameLabel.setBounds(173, 10, 382, 24);
	    appNameLabel.setText("AppName");
	    
	    urlLabel = new Label(composite, SWT.NONE);
	    urlLabel.setBounds(173, 40, 382, 24);
	    urlLabel.setText("www.example.com");
	    
	    compilationLabel = new Label(composite, SWT.SHADOW_NONE);
	    compilationLabel.setBounds(170, 70, 385, 24);
	    compilationLabel.setText("Remote");
	    
	    platformsLabel = new Label(composite, SWT.NONE);
	    platformsLabel.setBounds(170, 100, 385, 24);
	    platformsLabel.setText("Android ios BlackBerry WP7 WP8 Symbian");
	    
	    directoryLabel = new Label(composite, SWT.NONE);
	    directoryLabel.setBounds(173, 130, 382, 24);
	    directoryLabel.setText("/usr/path/to/my/home/directory/");
	    
	    iconLabel = new Label(composite, SWT.NONE);
	    iconLabel.setBounds(173, 190, 382, 24);
	    iconLabel.setText("/usr/path/to/the/icon/directory");
	    
	    offlineDirectoryLabel = new Label(composite, SWT.NONE);
	    offlineDirectoryLabel.setBounds(173, 220, 382, 24);
	    offlineDirectoryLabel.setText("/usr/path/to/offline/directory");
	    
	    Label lblArchivoDeConfiguracin = new Label(composite, SWT.NONE);
	    lblArchivoDeConfiguracin.setBounds(10, 160, 147, 24);
	    lblArchivoDeConfiguracin.setText("Setup file:");
	    
	    configDirectoryLabel = new Label(composite, SWT.NONE);
	    configDirectoryLabel.setBounds(173, 160, 382, 24);
	    configDirectoryLabel.setText("/usr/path/to/configure/file/directory");
	    
	    Label lblAlPulsarfinalizar = new Label(topLevel, SWT.NONE);
	    GridData gd_lblAlPulsarfinalizar = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 6);
	    gd_lblAlPulsarfinalizar.heightHint = 38;
	    lblAlPulsarfinalizar.setLayoutData(gd_lblAlPulsarfinalizar);
	    lblAlPulsarfinalizar.setText("Once you click on Finish, a console will be displayed. The necessary commands to \ngenerate the Apps with the selected settings will be executed along with its final output.");

	    setPageComplete(true);
	
}

	  
public void setLabels(String _appName, String _url, String _compilation, String _platforms, String _directory, String _ico, String _offlineDirectory, String _configDirectory){
	  
	  appNameLabel.setText(_appName);
	  urlLabel.setText(_url);
	  compilationLabel.setText(_compilation);
	  platformsLabel.setText(_platforms);
	  directoryLabel.setText(_directory);
	  iconLabel.setText(_ico);
	  offlineDirectoryLabel.setText(_offlineDirectory);
	  configDirectoryLabel.setText(_configDirectory);
	  
}


};