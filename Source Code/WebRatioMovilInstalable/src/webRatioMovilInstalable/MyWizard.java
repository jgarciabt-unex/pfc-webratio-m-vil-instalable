package webRatioMovilInstalable;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.wizard.Wizard;

public class MyWizard extends Wizard {

	Page1 page1 = new Page1();
	Page2 page2 = new Page2();
	Page3 page3 = new Page3();
	
	ResourceManager console = new ResourceManager();
	
	public MyWizard() {
		setWindowTitle("WebRatio Movil Instalable - Setup Wizard");
	}

	@Override
	public void addPages() {
		addPage(page1);
		addPage(page2);
		addPage(page3);
	}

public String getParams(){
	String params=null;

	params=page1.getDirectory()+" "+page2.getAppName();
	params+=page2.getCompile()+page2.getPlattforms();
	return params;
}

private String getDirectoryComplete(){
	return page1.getDirectory()+File.separator+page2.getAppName();
}

public String getURL(){	
	return page2.getURL();
}


 @Override
 public boolean performFinish() {
	 
	 console.setCommand(getParams());
	 console.setDirectory(getDirectoryComplete());
	 console.setAppName(page2.getAppName());
	 console.setUrl(getURL());
	 	if(page1.getSelectionConfig()){
	 		console.setConfigPat(page1.getConfig());}
	 	else{
	 		console.setConfigPat("");}
		if(page1.getSelectionIco()){
			console.setIcoPath(page1.getIco());}
		else{
			console.setIcoPath("");}
		if(page1.getSelectionHTML()){
			console.setOfflineResourcePath(page1.getOfflineResources());}
		else{
			console.setOfflineResourcePath("");}
	try {
		console.execute();
	} catch (IOException e) {
		e.printStackTrace();
	} 
	
	 return  page1.isPageComplete() && page2.isPageComplete() && page3.isPageComplete();
 }
 


}
