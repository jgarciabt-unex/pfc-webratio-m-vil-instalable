
@echo off 

set currentDirectory=%cd%

>output.tmp node --version
<output.tmp (
	set /p hasNode=
)

echo %hasNode% ¦ findstr "[vV][0-9.]*"
if ERRORLEVEL 1 (
	echo Error, "Node.js" is not installed. Please install it and try again. 
	pause
	exit
) else (
	echo Node is installed
)

cd %1
cd %2
cd res

findstr /r "3.[0-3].[0-9.-]*" version.tmp
if ERRORLEVEL 1 (
	echo Phonegap CLI is not installed. Installing Phonegap3.3 through Node.
	npm install -g phonegap@3.3
	npm install -g cordova@3.3
	pause
) else (
	echo Phonegap is installed
)

rem Move to target directory
cd %1

rem Create Phonegap Project
echo Creating the project...
call phonegap create %2

rem Move to application folder
cd %2

rem Install "Connection" plug-in on the current project.
call phonegap local plugin add https://git-wip-us.apache.org/repos/asf/cordova-plugin-network-information.git	
	
	
rem Copy all necesary files to application_name/www directoy before compile the apps.
rem Delete "index.html" file because the user is not allowed to edit it.
xcopy templates www /E /Q /Y
del templates\index.html
if exist tmp ( 
	xcopy tmp\icon.png www /E /Q /Y
	xcopy tmp\iconWWW www\res\icon /E /Q /Y
) 

rem Build the apps for each platforms.
echo Building apps...
for %%p in (%4 %5 %6 %7) do (
 	call phonegap %3 build %%p
)

if "%3" == "local" (
	rem If the user has selected "Custom Icon", change all the apps icons before build it again.
	if exist tmp (
		echo existeTMP
		if exist platforms\android (
			xcopy tmp\res platforms\android /E /Q /Y
		)
		if exist platforms\wp7 (
			copy tmp\iconWWW\windows-phone\icon-62-tile.png platforms\wp7\AplicationIcon.png
		)
		if exist platforms\wp8 (
			copy tmp\iconWWW\windows-phone\icon-62-tile.png platforms\wp8\AplicationIcon.png /E /Q /Y
		)
	)
	mkdir apps
	if exist platforms\android (
		call cordova build android --release
		xcopy platforms\android\bin\*.apk apps /E /Q /Y
	)
	if exist platforms\blackberry10 (
		call platforms\blackberry10\cordova\build --release
		xcopy platforms\blackberry10\build\device\bb10app.bar apps /E /Q /Y
	)
	if exist platforms\wp7 (
		call cordova build wp7 --release
		xcopy platforms\wp7\bin\Release\*.xap apps /E /Q /Y
	)
	if exist platforms\wp8 (
		call cordova build wp8 --release
		xcopy platforms\wp8\bin\Release\*.xap apps /E /Q /Y
	)	
)

if exist tmp (
		rmdir tmp /S /Q
)		

rmdir scripts /S /Q
	
pause

del res /S /Q

exit
